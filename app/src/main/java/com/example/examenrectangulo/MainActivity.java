package com.example.examenrectangulo;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {

    private EditText editTextName;
    private Button buttonEntrar, buttonExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inicializar vistas
        editTextName = findViewById(R.id.editTextName);
        buttonEntrar = findViewById(R.id.buttonEnter);
        buttonExit = findViewById(R.id.buttonExit);

        // Configurar oyente para el botón "Entrar"
        buttonEntrar.setOnClickListener(v -> {
            String name = editTextName.getText().toString();
            Intent intent = new Intent(MainActivity.this, RectanguloActivity.class);
            intent.putExtra("name", name);
            startActivity(intent);
        });

        // Configurar oyente para el botón "Salir"
        buttonExit.setOnClickListener(v -> finish());
    }
}