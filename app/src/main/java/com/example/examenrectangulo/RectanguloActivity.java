package com.example.examenrectangulo;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class RectanguloActivity extends AppCompatActivity {

    private TextView textViewName;
    private EditText editTextBase, editTextAltura, editTextArea, editTextPerimetro;
    private Button buttonCalcular, buttonRegresar, buttonLimpiar;
    private Rectangulo rectangulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);

        // Inicializar vistas
        textViewName = findViewById(R.id.textViewName);
        editTextBase = findViewById(R.id.editTextBase);
        editTextAltura = findViewById(R.id.editTextAltura);
        editTextArea = findViewById(R.id.editTextArea);
        editTextPerimetro = findViewById(R.id.editTextPerimetro);
        buttonCalcular = findViewById(R.id.buttonCalcular);
        buttonRegresar = findViewById(R.id.buttonRegresar);
        buttonLimpiar = findViewById(R.id.buttonLimpiar);

        // Obtener el nombre pasado desde MainActivity y establecerlo en el TextView
        String name = getIntent().getStringExtra("name");
        textViewName.setText("Mi nombre es " + name);

        // Configurar el listener para el botón "Calcular"
        buttonCalcular.setOnClickListener(v -> {
            try {
                double base = Double.parseDouble(editTextBase.getText().toString());
                double altura = Double.parseDouble(editTextAltura.getText().toString());

                // Crear instancia de Rectangulo y calcular área y perímetro
                rectangulo = new Rectangulo(base, altura);
                double area = rectangulo.calcularArea();
                double perimetro = rectangulo.calcularPerimetro();

                // Establecer los resultados en los EditText correspondientes
                editTextArea.setText(String.valueOf(area));
                editTextPerimetro.setText(String.valueOf(perimetro));
            } catch (NumberFormatException e) {
                // Manejar una excepción si los datos ingresados no son números válidos
                editTextArea.setText("Error");
                editTextPerimetro.setText("Error");
            }
        });

        // Configurar el listener para el botón "Regresar"
        buttonRegresar.setOnClickListener(v -> finish());

        // Configurar el listener para el botón "Limpiar"
        buttonLimpiar.setOnClickListener(v -> {
            // Limpiar todos los EditText
            editTextBase.setText("");
            editTextAltura.setText("");
            editTextArea.setText("");
            editTextPerimetro.setText("");
        });
    }
}