package com.example.examenrectangulo;

public class Rectangulo {
    private double base;
    private double altura;

    // Constructor
    public Rectangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    // Getter para la base
    public double getBase() {
        return base;
    }

    // Setter para la base
    public void setBase(double base) {
        this.base = base;
    }

    // Getter para la altura
    public double getAltura() {
        return altura;
    }

    // Setter para la altura
    public void setAltura(double altura) {
        this.altura = altura;
    }

    // Método para calcular el área
    public double calcularArea() {
        return base * altura;
    }

    // Método para calcular el perímetro
    public double calcularPerimetro() {
        return 2 * (base + altura);
    }

    // Método para limpiar los valores
    public void limpiar() {
        this.base = 0;
        this.altura = 0;
    }
}
